# Websocket Echo Server

Built with:

- [ws](https://github.com/websockets/ws)
- [rxjs](https://rxjs.dev/)
- [typescript](https://www.typescriptlang.org/)
