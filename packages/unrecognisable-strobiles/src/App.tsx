import React from 'react'
import {css} from '@emotion/css'

import {Id, StageBlock} from './data/types'

import Block from './components/Block'
import * as Styled from './components/Styled'

import {log} from './lib/utilities'
import {StageModelContext} from './lib/context'
import { makeStageModel } from './lib/helpers'

function App() {
  var [canEdit, setCanEdit] = React.useState(false)
  var toggleCanEdit = () => { setCanEdit(!canEdit) }

  var [editing, setEditing] = React.useState<string|undefined>(undefined)
  var onClick = (event: React.MouseEvent, blockId?: Id) => {
    log('CONTAINER_CLiCK', event, blockId)
    setEditing(blockId)
  }

  var {state} = React.useContext(StageModelContext)
  var content = makeStageModel(state)

  return (
    <Styled.Container>
      {content.blocks.map((b:StageBlock) => <Block key={b.id} id={b.id} elements={b.elements} className={css(b.style)} setEdit={onClick} readonly={canEdit ? b.id !== editing : true} />)}
      <Styled.EditButton onClick={toggleCanEdit} canEdit={canEdit}>{canEdit ? 'Lock' : 'Edit'}</Styled.EditButton>
    </Styled.Container>
  )
}

export default App
