import React from "react";
import { BaseEditor } from "slate";
import { ReactEditor } from "slate-react";

export type Id = string;
type Css = string;
type Layout = Css;

type Identifiable = { id: Id }
type StageText = { text: string }

export type StageElement = Identifiable & (TextElement | ImageElement)
export type StageBlock = Identifiable & {
  type: 'block',
  elements: StageElement[],
  style?: Layout
}

type TextElement = Identifiable & {
  type: "paragraph" | "title",
  children: StageText[]
}

type ImageElement = Identifiable &
  React.HTMLProps<HTMLImageElement> & {
    type: "image",
    children: { text: "" }[]
  }

export type StageItem = StageElement | StageBlock

export type BlockState = { id: Id, elements: Id[] }
export type StageState = {
  id: Id,
  data: Record<Id, StageItem>,
  layout: {
    blocks: BlockState[]
  }
}

export type StageModel = {
  id: Id,
  blocks: StageBlock[]
}

declare module "slate" {
  interface CustomTypes {
    Editor: BaseEditor & ReactEditor;
    Element: StageElement;
    Text: StageText;
  }
}
