import { nanoid } from "nanoid";
import { Id, StageItem, StageState } from './types';

const STAGE: Id = nanoid();
const TITLE: Id = nanoid();

const FIRST_ELEMENT: Id = nanoid();
const SECOND_ELEMENT: Id = nanoid();
const THIRD_ELEMENT: Id = nanoid();
const FOURTH_ELEMENT: Id = nanoid();

const TITLE_BLOCK: Id = nanoid(); const FIRST_BLOCK: Id = nanoid();
const SECOND_BLOCK: Id = nanoid();
const THIRD_BLOCK: Id = nanoid();

export const initialState: StageState = {
  id: STAGE,
  layout: {
    blocks: [
      {
        id: TITLE_BLOCK,
        elements: [TITLE]
      },
      {
        id: FIRST_BLOCK,
        elements: [FIRST_ELEMENT]
      },
      {
        id: SECOND_BLOCK,
        elements: [SECOND_ELEMENT, THIRD_ELEMENT]
      },
      {
        id: THIRD_BLOCK,
        elements: [FOURTH_ELEMENT]
      }
    ]
  },
  data: {
    [TITLE]: {
      id: TITLE,
      type: "title",
      children: [{ text: "title" }]
    },
    [FIRST_ELEMENT]: {
      id: FIRST_ELEMENT,
      type: "paragraph",
      children: [
        {
          text:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Proin tortor purus platea sit eu id nisi litora libero. Neque vulputate consequat ac amet augue blandit maximus aliquet congue. Pharetra vestibulum posuere ornare faucibus fusce dictumst orci aenean eu facilisis ut volutpat commodo senectus purus himenaeos fames primis convallis nisi."
        }
      ]
    },
    [SECOND_ELEMENT]: {
      id: SECOND_ELEMENT,
      type: "paragraph",
      children: [
        {
          text:
            "Phasellus fermentum malesuada phasellus netus dictum aenean placerat egestas amet. Ornare taciti semper dolor tristique morbi. Sem leo tincidunt aliquet semper eu lectus scelerisque quis. Sagittis vivamus mollis nisi mollis enim fermentum laoreet."
        }
      ]
    },
    [THIRD_ELEMENT]: {
      id: THIRD_ELEMENT,
      type: "image",
      children: [{ text: "" }],
      src: "https://picsum.photos/200?random=1",
      alt: "placeholder"
    },
    [FOURTH_ELEMENT]: {
      id: FOURTH_ELEMENT,
      type: "image",
      children: [{ text: "" }],
      src: "https://picsum.photos/200?random=2",
      alt: "placeholder"
    },
    [FIRST_BLOCK]: {
      id: FIRST_BLOCK,
      type: 'block',
    },
    [SECOND_BLOCK]: {
      id: SECOND_BLOCK,
      type: 'block',
      style: `
        display: flex;
        > * {
          width: 50%;
        }
      `
    },
    [THIRD_BLOCK]: {
      type: 'block',
      id: THIRD_BLOCK,
      style: "> * {width: 100%;}"
    }
  } as Record<Id, StageItem>
};
