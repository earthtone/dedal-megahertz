import {BlockState, Id, StageBlock, StageModel, StageState} from './data/types'

export function makeStageModel ({ id, data, layout }: StageState): StageModel {
  var getItem = (itemId: Id) => data[itemId]
  var getBlocks = (b: BlockState) => ({ ...getItem(b.id), elements: b.elements.map(getItem) })

  var blocks = layout.blocks.map(getBlocks) as StageBlock[]

  return {
    id,
    blocks
  }
}

