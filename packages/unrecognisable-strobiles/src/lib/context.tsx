import React from 'react';
import {StageState} from '../data/types';
import { initialState } from '../data/sample'


type State = StageState & {}
type ActionType = unknown
type Action = {
  type: ActionType,
  payload: any
}

function reducer (state: State, action: Action) : State {
  switch (action.type) {
    default:
      return state
  }
}

export const StageModelContext =
  React.createContext<{ state: State, dispatch: React.Dispatch<Action> }>({ state: initialState, dispatch: () => {} });
