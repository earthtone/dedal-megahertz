export function exclude<A, B>(
  key: keyof A
): (r: Record<keyof A | keyof B, A | B>) => Record<keyof B, B> {
  return (r) => {
    let { [key]: A, ...rest } = r;
    return rest as Record<keyof B, B>;
  };
}

export function zip<A, B>(as: A[], bs: B[]): Array<[A, B]> {
  return as.map((a, i) => [a, bs[i]]);
}

export function log(tag: string, ...args: unknown[]) {
  let styleString = "padding:0.5rem;background-color:black;color:white;";
  console.group(`%c${tag}`, styleString);
  args.forEach((s) => {
    console.log(s);
  });
  console.groupEnd();
}
