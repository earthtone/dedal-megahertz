import React from 'react'
import styled from '@emotion/styled'

const Main = styled.main`
  width: 72ch;
  margin: 0 auto;
  padding: 1rem;

  article > * {
    margin: 1rem 0;
  }
`

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`

export function Container ({ children, ...props } : {children: React.ReactNode}) : JSX.Element {
  return (
    <Wrapper {...props}>
      <Main>
        {children}
      </Main>
    </Wrapper>
  )
}

export const  EditButton = styled.button<{ canEdit: boolean }>`
  padding: 0.25rem 0.5rem;
  border-radius: 2.5rem;
  width: 3.5rem;

  background-color: ${(props) => props.canEdit ? 'black' : 'white' };
  border: 1px solid ${(props) => props.canEdit ? 'white' : 'black' };
  color: ${(props) => props.canEdit ? 'white' : 'black' };

  position: fixed;
  bottom: 1rem;
  left: 1rem;
`
