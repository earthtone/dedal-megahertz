import * as React from "react";
import { createEditor, Descendant } from "slate";
import { Slate, withReact } from "slate-react";

import { StyledEditable, renderElement } from "./elements";
import { log } from "../../lib/utilities";
import {Id, StageBlock} from "../../data/types";

type Props = Omit<StageBlock, 'type'> & {
  setEdit: (e: React.MouseEvent, id?: Id) => void;
  readonly: boolean;
  id: Id;
}

export default function Block({
  elements,
  setEdit,
  id,
  readonly,
  className
}: Props & React.HTMLAttributes<HTMLElement>) {
  const editor = React.useMemo(() => withReact(createEditor()), []);
  const [value, setValue] = React.useState<Descendant[]>(elements);
  const onChange = (v: Descendant[]) => {
    setValue(v);
  };

  const _renderElement = React.useCallback(renderElement, []);
  const onClick = (event: React.MouseEvent) => {
    event.stopPropagation();
    setEdit(event, id);
    log("BLOCK_STATE", id, readonly);
  };

  return (
    <div onClick={onClick}>
      <Slate editor={editor} value={value} onChange={onChange}>
        <StyledEditable
          readOnly={readonly}
          renderElement={_renderElement}
          className={className}
        />
      </Slate>
    </div>
  );
}
