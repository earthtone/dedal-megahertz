import * as React from 'react'
import styled from "@emotion/styled";
import { RenderElementProps, Editable } from 'slate-react'
import {exclude} from '../../lib/utilities'

export const Title = styled.h1`
  text-transform: capitalize;
`;

export const StyledEditable = styled(Editable)`
  border: ${(props) =>
    props.readOnly ? "1px dashed transparent" : "1px dashed #ccc"};
  padding: 0.5rem;
  > * {
    margin: 0.5rem;
  }
`;

const getElAttrs = exclude('children')

function DefaultElement (props: RenderElementProps) {
  return <p {...props.attributes}>{props.children}</p>
}

function TitleElement (props: RenderElementProps) {
  return <Title {...props.attributes}>{props.children}</Title>
}

function ImageElement ({ children, ...props }: RenderElementProps) {
  return <img {...getElAttrs(props.element)} />
}

export function renderElement(props: RenderElementProps) {
  switch (props.element.type) {
    case 'image':
      return <ImageElement {...props} />
    case 'title':
      return <TitleElement {...props} />
    default:
      return <DefaultElement {...props} />
  }
}

