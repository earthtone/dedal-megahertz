# Toy Editor Test

Built with:

- [vite](https://vitejs.dev/)
- [react](https://reactjs.org/)
- [typescript](https://www.typescriptlang.org/)
- [slatejs](https://docs.slatejs.org/)
- [emotion](https://emotion.sh/docs/introduction)
